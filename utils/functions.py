import plotly.express as px
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import CubicSpline
from scipy.spatial.transform import Rotation as R


'''
This file contains the functions required for running the shape sensing notebooks. 
To use these functions, ensure that your virtual environment has the requirement.txt file installed:
$ pip3 install -r requirements.txt

List of the functions:

    read_strain(file) - Read CSV microstrain data file and return a python array 
                      -> return eps
    
    reject_outliers(data, dx, m=1)  -

    smoothen_strain(cores: np.ndarray, kernel_size: int = 5) -
    
    get_ss_variables (spl_eps, ds, r_in) - Compute the variable theta, K, phi, R and tau using all core strain measurements at a 
                                           given time step
                                         -> return theta, K, phi, R, tau
    
    Frenet_Serret(ds, K, tau, T_0, N_0, r_0) - Return the 3D curve derived from the K and tau variable arrays at a given time 
                                               step
                                             ->  return r
                                               
    Matrix_Transform(theta, R, phi) - Return the 3D curve derived from the phi, R and theta variable arrays at a given time step
                                    > return P
                                    
    

List of variables
file - input csv file containing straining measurements 
length - 
eps -
ds -
spl_eps -
ds -
r_in -
K -
tau -
phi -
theta -
R -
r0 -
r - 
T_0 -
T -
N_0 -
N -
B_0 -
B -

'''


def read_strain(file: str) -> np.ndarray:
    # Read CSV file using pandas and specify tab as the separator
    df = pd.read_csv(file, sep="\t")

    # Interpolate NaN values in the data
    for row in df:
        df[row] = pd.to_numeric(df[row], errors='coerce')
    df.index = pd.DatetimeIndex(df.index)
    df_int = df.interpolate(method='time', axis=0)

    # Convert pandas dataframe to numpy array with dtype float64
    eps = df_int.to_numpy(dtype=np.float64)
    eps = np.nan_to_num(eps, nan=1e15, posinf=1e15, neginf=1e-15)
    
    #check that NaNs have been successfully removed
    if np.isnan(eps).any() == True:
        print("NaNs still present in the data")
    
    # Return the numpy array with interpolated values
    return eps


def reject_outliers(data, dx, m=1):
    # Repeat until no more outliers are found
    restart = True
    while restart:  
        restart = True
        break_flag = False

       
        for i in range(data.shape[0]):  
            
            # Calculate gradient of each row
            grad = np.gradient(data[i, :], dx)

            for j in range(data.shape[1] - 1): 
               
                # Check if gradient is too high
                if abs(grad[j]) > 1e6*m:  
                    
                    # Replace the value with the previous one in the same row
                    data[i, j + 1] = data[i, j]

                    break_flag = True
                    break
                    
                # If it reaches the last column of the row
                if j == data.shape[1] - 2: 
                    if i == data.shape[0] - 1:  # and it's the last row
                        restart = False  # stop the loop
                        continue
                        exit()  

                    else:  # if it's not the last row
                        restart = True  # continue the loop
                        break_flag = False

                if break_flag:  # if an outlier is found
                    break
            if break_flag:  # if an outlier is found
                break
                
    # Return the cleaned data
    return data  


def smoothen_strain(cores: np.ndarray, kernel_size: int = 5) -> tuple:
    # Get dimensions of input array
    nt = cores.shape[1] # number of time steps
    ns = cores.shape[2] # number of sensor readings

    # Create output arrays
    sc1 = np.zeros((nt, ns))
    sc2 = np.zeros((nt, ns))
    sc3 = np.zeros((nt, ns))

    # Create a 1D smoothing kernel
    kernel = np.ones(kernel_size) / kernel_size

    # Apply convolution to each time step for each sensor reading
    for t in range(nt):
        sc1[t,:] = np.convolve(cores[0,t,:], kernel, mode='same')
        sc2[t,:] = np.convolve(cores[1,t,:], kernel, mode='same')
        sc3[t,:] = np.convolve(cores[2,t,:], kernel, mode='same')

    # Return the smoothed arrays as a tuple
    return sc1, sc2, sc3


def get_ss_var(spl_eps, ds, r_in):
    n_cores = spl_eps.shape[0]   # number of cores in splines
    n_measurements = spl_eps.shape[1]   # number of measurements

    # Initialize arrays
    theta = np.zeros([n_measurements])
    K = np.zeros([n_measurements])

    # Angular distance of cores from horizontal axis (in radians)
    theta_cores = np.zeros((n_cores))

    # Compute the angular position of each core
    for i in range(n_cores):
        theta_cores[i] = ((2 * np.pi * i) / n_cores)
    
    # Compute the angle increment for each measurement and add it to the angular position of each core 
    # This effect is due to core twisting around JIP-CALM sensor
    angle_increment = (np.deg2rad(360) / n_measurements)

    for i in range(n_measurements):
        theta_cores += angle_increment
        
        # Compute the core location matrix M
        M = np.zeros([3, 3])
        for j in range(n_cores):
            M[j, :] = np.array([-r_in * np.cos(theta_cores[j]), r_in * np.sin(theta_cores[j]), 1])

        # Compute the pseudo-inverse of M
        cross = np.linalg.pinv(M)

        # Compute the dot product of the location matrix and the strain
        alpha = np.dot(cross, spl_eps[:, i])

        # Compute the angle of bending theta
        theta[i] = np.arctan(alpha[1] / alpha[0])
      
        # Compute the curvature K
        K[i] = np.sqrt(alpha[0] ** 2 + alpha[1] ** 2)
     
    # Compute the direction of bending tau
   # theta = np.unwrap(theta,  period=np.pi) 
    tau = np.gradient(np.abs(theta), ds)
   # tau = np.gradient(np.abs(theta), ds)
  #  tau = np.gradient(np.unwrap(theta, period=np.pi), ds)

    # Compute the radius of curvature R
    R = 1 / K

    # Compute the twist angle phi
    phi = ds / R

    # Return variable arrays
    return theta, K, phi, R, tau


def Frenet_Serret(ds: float, K: np.ndarray, tau: np.ndarray, T_0: np.ndarray, N_0: np.ndarray, r_0: np.ndarray) -> np.ndarray:
    n = len(K) # number of measurements

    # initial conditions
    B_0 = np.cross(T_0, N_0)

    # Initialize arrays for storing vectors at n+1 points along the curve
    T = np.zeros([n + 1, 3])
    N = np.zeros([n + 1, 3])
    B = np.zeros([n + 1, 3])
    r = np.zeros([n + 1, 3])

    # Set initial values
    T[0, :] = T_0
    N[0, :] = N_0
    B[0, :] = B_0
    r[0, :] = r_0

    # Solve Frenet-Serret equations
    for i in range(n):
        # Get curvature and torsion at i-th point
        k = K[i]
        t = tau[i]

        # Update normal vector using Frenet-Serret equations
        N[i + 1, :] = (ds * t * B[i, :] - ds * k * T[i, :] + N[i, :]) / (1 + ds * ds * t * t + ds * ds * k * k)

        # Update tangent and binormal vectors using Frenet-Serret equations
        T[i + 1, :] = ds * k * N[i + 1, :] + T[i, :]
        B[i + 1, :] = -ds * t * N[i + 1, :] + B[i, :]

        # Update position vector using Frenet-Serret equations
        r[i + 1, :] = ds * T[i + 1, :] + r[i, :]
        
    # Return 3D positions of points along the curve
    return r


def Matrix_Transform(theta, R, phi, ds):
    n = len(R) # number of measurements

    ### reconstruct shape using homogeneous transformation matrices
    P = np.zeros([n, 4])
    A = np.zeros([n, 4, 4])
    Acum = np.zeros([n, 4, 4])
    P_0 = [0, 0, 0, 1]

    for i in range(n):
        t = theta[i]*ds
        p = phi[i]
        r = R[i]

        ct = np.cos(t)
        st = np.sin(t)
        cp = np.cos(p)
        sp = np.sin(p)

        A_1 = [ct * cp, -st, ct * sp, r * (1 - cp) * ct]
        A_2 = [st * cp, ct, st * sp, r * (1 - cp) * st]
        A_3 = [-sp, 0, cp, r * sp]
        A_4 = [0, 0, 0, 1]
        A[i, :, :] = [A_1, A_2, A_3, A_4]

        if i == 0:
            Acum[i, :, :] = A[i, :, :]
        else:
            Acum[i, :, :] = np.matmul(Acum[i - 1, :, :], A[i, :, :])

        P[i, :] = np.matmul(Acum[i, :, :], P_0)
    return P


def path_info(P, name):
    P_length = 0
    for i in range(P.shape[0] - 1):
        P_length += np.linalg.norm(P[i + 1, :] - P[i, :])
    print("Shape of", name, P.shape, "with physical length", P_length)
    return

def rotate_curve(Curve):
    # Extract last point of curve 
    last_point = Curve[-1]
    
    # Define last point constrained at z=0
    rotated_last_point = np.array([last_point[0],last_point[1],0])
    
    # Compute the angle between both vectors
    unit_vector_1 = last_point / np.linalg.norm(last_point)
    unit_vector_2 = rotated_last_point / np.linalg.norm(rotated_last_point)

    dot_product = np.dot(unit_vector_1, unit_vector_2)

    angle = np.arccos(dot_product)
    
    # Rotate the entire curve by the angle
    r = R.from_euler('y',angle)
    rotated_curve = r.apply(Curve)
    
    return rotated_curve

