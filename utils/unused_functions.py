def interpolate_strain(length, eps, ds):
    n_cores = eps.shape[0]  # virtual number of measurements
    n_measurements = eps.shape[1]
    spl_eps = np.zeros((n_cores, n_measurements * length))

    ds = ds / length  # adjust the virtual distance between measurements

    # interpolate the strain using univariate spline
    x = np.linspace(0, n_measurements, n_measurements)
    xs = np.linspace(0, n_measurements, n_measurements * length)
    for i in range(n_cores):
        spl_eps[i, :] = CubicSpline(x, eps[i, :])(xs)

    # plots of interpolation
    plt.title('core 1 strain interpolation (m/m)')
    plt.plot(x, eps[0, :], 'ro', ms=5)
    plt.plot(xs, spl_eps[0, :], 'g', lw=2, alpha=0.9)
    plt.show()
    plt.cla()

    plt.title('core 2 strain interpolation (m/m)')
    plt.plot(x, eps[1, :], 'ro', ms=5)
    plt.plot(xs, spl_eps[1, :], 'g', lw=2, alpha=0.9)
    plt.show()
    plt.cla()

    plt.title('core 3 strain interpolation (m/m)')
    plt.plot(x, eps[2, :], 'ro', ms=5)
    plt.plot(xs, spl_eps[2, :], 'g', lw=2, alpha=0.9)
    plt.show()
    return spl_eps, ds

def theta_cores(r_in, n_cores):
    n_cores = 3
    r_in = 2 * (10 ** -3)
    theta_cores = np.zeros((n_cores))
    for i in np.arange(n_cores):
        theta_cores[i] = ((2 * np.pi) / n_cores) * i

    return theta_cores

def get_rTNB(P):
    r0 = P[0, :]
    dP = np.apply_along_axis(np.gradient, axis=0, arr=P)
    ddP = np.apply_along_axis(np.gradient, axis=0, arr=dP)

    f = lambda m: m / np.linalg.norm(m)  # calculate normalized tangents
    T = np.apply_along_axis(f, axis=1, arr=dP)

    B = np.cross(dP, ddP)  # calculate normalized binormal
    B = np.apply_along_axis(f, axis=1, arr=B)

    N = np.cross(B, T)  # calculate normalized normals
    return r0, T, N, B


def align_curves(P1, P2, ds):  # aligning P2 to TNB frame of P1[0] at origin
    r1, T1, N1, B1 = get_rTNB(P1)
    r2, T2, N2, B2 = get_rTNB(P2)

    F1 = [T1[0, :], N1[0, :], B1[0, :]]  # extract the TNB at origin
    F2 = [T2[0, :], N2[0, :], B2[0, :]]

    R = np.dot(np.linalg.inv(F2), F1)  # compute rotation matrix between TNB axese

    r_out = np.dot(P2, R)  # rotate curve P2

    return r_out
